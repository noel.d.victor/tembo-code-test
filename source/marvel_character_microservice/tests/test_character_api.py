import unittest
import json
import requests


class TestCase(unittest.TestCase):

    def test_get_characters(self):
        rv = requests.get('http://marvel_microservice:5000/api/marvel_characters/')
        self.assertEqual(rv.status_code, 200)
        self.assertIn("Spider-Man", rv.text)
        self.assertIn("Count von Blitzkrieg", rv.text)

    def test_crud_character(self):
        test_guy = {
            "alignment": "hero",
            "alive": "Alive",
            "as_of_year": "2018",
            "at_large": "",
            "crimes_committed": "",
            "estimated_damages": "",
            "extra": "",
            "eye": "",
            "first_appearance": "",
            "hair": "",
            "identity": "",
            "marvel_universe": "",
            "name": "Batman",
            "nickname": "Caped Crusader",
            "sex": "Male",
            "total_appearances": "Alot"
        }
        payload = json.dumps(test_guy)
        # test create
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        rv = requests.post(
            'http://marvel_microservice:5000/api/marvel_characters/',
            data=payload,
            headers=headers
        )
        self.assertIn("Batman", rv.text)
        self.assertIn("Caped Crusader", rv.text)

        # test get
        hero_id = rv.json()['id']
        rv = requests.get('http://marvel_microservice:5000/api/marvel_characters/{}'.format(hero_id))
        self.assertEqual(rv.status_code, 200)
        self.assertIn("Batman", rv.text)

        # test update
        test_guy["nickname"] = "Dark Knight"
        payload = json.dumps(test_guy)
        rv = requests.put(
            'http://marvel_microservice:5000/api/marvel_characters/{}'.format(hero_id),
            data=payload,
            headers=headers
        )
        self.assertIn("Dark Knight", rv.text)

        # test delete
        payload = json.dumps(test_guy)
        rv = requests.delete(
            'http://marvel_microservice:5000/api/marvel_characters/{}'.format(hero_id),

        )
        self.assertIn("Deleted", rv.text)
        self.assertIn(hero_id, rv.text)


if __name__ == '__main__':
    unittest.main()
