import csv
import re

from .base_adapter import BaseAdapter
from .exceptions import FileAssumptionsChangedError
from .exceptions import InvalidRowError


class CharacterAdapter(BaseAdapter):
    # Abstract adapter so no slug
    slug = None

    standard_model = [
        'id',
        'name',
        'nickname',
        'extra',
        'identity',
        'marvel_universe',
        'alignment',
        'eye',
        'hair',
        'sex',
        'alive',
        'first_appearance',
        'at_large',
    ]

    def _check_line(self, orig_line, line):
        count_value = 0
        for value in orig_line.values():
            if value.strip():
                count_value += 1
        if count_value <= 2:
            raise InvalidRowError()

    def clean_id(self, line):
        value = line['id']
        return value

    def clean_name(self, line):
        value = line['name']
        value = value.split("(")[0]
        return value

    def clean_extra(self, line):
        orig_value = line['name']
        value = ""
        matchs = re.findall(r"\((.*?)\)", orig_value)

        for match in matchs:
            if "Earth" not in match:
                value = match

        return value

    def clean_nickname(self, line):
        orig_value = line['name']
        value = ""
        match = re.search('\\\"(.*?)\\"', orig_value)
        if match:
            value = match.group(1)
            value = value.replace("\\", "")
        return value

    def clean_identity(self, line):
        value = line['IDENTITY']
        value = value.replace("Identity", "")
        return value

    def clean_marvel_universe(self, line):
        orig_value = line['name']
        value = ""
        match = re.search(r"\(Earth-(\w+)\)", orig_value)
        if match:
            value = "Earth-{}".format(match.group(1))

        return value

    def clean_eye(self, line):
        value = line['EYE']
        return value

    def clean_hair(self, line):
        value = line['HAIR']
        if value == "Reddish Blond Hair":
            # These are about the same
            # Choose the one with more uses as canon
            value = "Strawberry Blond Hair"
        return value

    def clean_sex(self, line):
        orig_value = line['SEX']
        value = ""
        if "Characters" in orig_value:
            value = orig_value.replace("Characters", "")
        return value

    def clean_alive(self, line):
        orig_value = line['ALIVE']
        value = ""
        if "Characters" in orig_value:
            value = orig_value.replace("Characters", "")
        return value

    def clean_first_appearance(self, line):
        value = ""
        month_year = line['FIRST APPEARANCE']
        year = line['Year']
        if month_year:
            month = month_year.split("-")[0]
            value = month + "-" + year
        return value
