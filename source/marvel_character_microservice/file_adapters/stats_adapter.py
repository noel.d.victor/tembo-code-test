import csv
import re

from .base_adapter import BaseAdapter
from .exceptions import FileAssumptionsChangedError
from .exceptions import InvalidRowError


class StatsAdapter(BaseAdapter):
    slug = "stats_adapter_v1"
    standard_model = [
        'id',
        'crimes_committed',
        'estimated_damages',
        'total_appearances',
        'as_of_year'
    ]

    def _check_file_assumptions(self):
        """
        Check our assumptions about file are still valid

        """
        super(StatsAdapter, self)._check_file_assumptions()
        expected_fields = [
            "entity_id",
            "metric_id",
            "year",
            "value",
        ]

        with open(self.temp_file, "r") as csvfile:
            reader = csv.reader(csvfile)
            fields = next(reader)

        for field in fields:
            if field not in expected_fields:
                raise FileAssumptionsChangedError()

    def clean_id(self, line):
        value = line['entity_id']
        return value

    def clean_crimes_committed(self, line):
        metric = line['metric_id']
        value = ""
        if 'crimes_committed' in metric:
            value = line['value']
        return value

    def clean_estimated_damages(self, line):
        metric = line['metric_id']
        value = ""
        if 'estimated_damages' in metric:
            value = line['value']
        return value

    def clean_total_appearances(self, line):
        metric = line['metric_id']
        value = ""
        if "total_appearances" in metric:
            value = line['value']
        return value

    def clean_as_of_year(self, line):
        value = line['year']
        return value
