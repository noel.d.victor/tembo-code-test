import csv
import re

from .exceptions import FileAssumptionsChangedError
from .exceptions import InvalidRowError

from .character_adapter import CharacterAdapter


class HeroAdapter(CharacterAdapter):
    slug = "hero_adapter_v1"

    def _check_file_assumptions(self):
        """
        Check our assumptions about file are still valid

        """
        super(CharacterAdapter, self)._check_file_assumptions()
        expected_fields = ["id",
                           "name",
                           "IDENTITY",
                           "ALIGN",
                           "EYE",
                           "HAIR",
                           "SEX",
                           "ALIVE",
                           "FIRST APPEARANCE",
                           "Year"]

        with open(self.temp_file, "r") as csvfile:
            reader = csv.reader(csvfile)
            fields = next(reader)

        for field in fields:
            if field not in expected_fields:
                raise FileAssumptionsChangedError()

    def clean_alignment(self, line):
        orig_value = line['ALIGN']
        value = ""
        if "good" in orig_value.lower():
            value = "hero"
        if "neutral" in orig_value.lower():
            value = "neutral"

        return value

    def clean_at_large(self, line):
        value = ""
        return value
