"""
This doesn
"""

import traceback
import logging


class BaseParsingError(Exception):
    """
    All errors for parsing errors should inherit from this
     error, so it becomes easy to catch any  Parsing Errors exceptions
    """
    user_friendly_msg = "Contact an Admin"
    traceback = None

    def __str__(self):
        return repr(self)


#### File Level Errors Start

class MissingFileError(BaseParsingError):
    def __init__(self, filename, msg=None):
        self.traceback = traceback.format_exc()
        if msg is None:
            # Set some default useful error message
            msg = "File : {}  is missing.".format(
                filename
            )
        super(MissingFileError, self).__init__(msg)


class FileFormatError(BaseParsingError):
    def __init__(self, file_format, supported_types, msg=None):
        if msg is None:
            msg = "Unsupported file format {0}. Please use one of {1}.".format(
                file_format,
                supported_types,
            )
        super(FileFormatError, self).__init__(msg)
        self.file_format = file_format
        self.supported_types = supported_types


class FileAssumptionsChangedError(BaseParsingError):
    def __init__(self, msg=None):
        if msg is None:
            msg = "File Assumptions have changed. Needs Manual Review"
        super(FileAssumptionsChangedError, self).__init__(msg)


class NormalizationFailedError(BaseParsingError):
    def __init__(self, msg=None):
        if msg is None:
            msg = "No rows could be cleaned."
            super(NormalizationFailedError, self).__init__(msg)


class MissingCleanFuncError(BaseParsingError):
    def __init__(self, data_field, msg=None):
        if msg is None:
            msg = "Clean Function for {} is Missing".format(data_field)
            super(MissingCleanFuncError, self).__init__(msg)


### File Level Errors End


### Line parsing Errors

class InvalidRowError(BaseParsingError):
    def __init__(self, msg=None):
        if msg is None:
            """
            Invalid Row Found
            """
        super(InvalidRowError, self).__init__(msg)


class NoKnownFileHeaders(BaseParsingError):
    def __init__(self, msg=None):
        if msg is None:
            msg = "File Adapter has no known headers. " \
                  "Double check File Adapter Attributes"
        super(NoKnownFileHeaders, self).__init__(msg)
