import csv
import traceback
import copy

from . import utils
from . import constants
from . import exceptions as parse_exceptions


class BaseAdapter(object):
    """
    This is the base adapter.
    Basically an abstract base class.
    This should be inherited by file specific adapters

    The main purpose of adapters are

    1) check if file is valid based on assumptions
    2) normalize and clean data in normalized cached csv

    Create a normalized file in same dir as file

    Then a 2nd file for file errors
    # Possible Dupe
    # Possible Failures
    # Ignored rows

    """

    # The slug used in other parts of system to diff. adapter
    slug = None

    # the standard data model we are processing to
    standard_model = NotImplementedError

    def __init__(self, original_file, verbose=False):

        self.file_name = original_file
        self.temp_file = utils.export_file_to_tmp(self.file_name)
        self.original_mimetype = utils.get_mimetype(self.file_name)
        self.status = constants.ETL_FILE_UNPROCESSED
        self.verbose = verbose

    def _check_file_assumptions(self):
        """
        Check our assumptions about file are still valid

        """
        if self.original_mimetype not in constants.CSV_MIMETYPES:
            raise parse_exceptions.FileFormatError(self.original_mimetype,
                                                   constants.CSV_MIMETYPES
                                                   )

    def _check_clean_methods(self):
        for data_field in self.standard_model:
            clean_func = getattr(self, "clean_{}".format(data_field), None)
            if not clean_func:
                raise parse_exceptions.MissingCleanFuncError(data_field)

    def _check_line(self, orig_line, line):
        """ Write checks on the orig line or cleaned line"""
        pass

    def _common_clean(self, value):
        value = value.strip()
        return value

    def _clean_line(self, line):
        output_line = {}
        for data_field in self.standard_model:
            clean_func = getattr(self, "clean_{}".format(data_field))
            cleaned_value = clean_func(line)
            cleaned_value = self._common_clean(cleaned_value)
            output_line[data_field] = cleaned_value
        return output_line

    def _clean_lines(self, lines):
        output_lines = []
        normalization_percentage = 0.0
        total_lines_count = len(lines)
        processed_line_count = 0
        row_errors = []

        for line in lines:
            copy_line = copy.deepcopy(line)
            try:
                output_line = self._clean_line(copy_line)
                self._check_line(line, output_line)
                output_lines.append(output_line)
            except Exception as e:
                trace = traceback.format_exception(None,
                                                   e, e.__traceback__),
                error_row = {
                    'error': e,
                    'traceback': trace,
                    'original_line_no': processed_line_count + 1,
                    'uncleaned_line': line
                }
                row_errors.append(error_row)

            # update status
            processed_line_count += 1
            normalization_percentage = round(processed_line_count * 100 / total_lines_count, 2)
            if self.verbose and normalization_percentage % 5 == 0:
                print("Cleaning: {}% ".format(normalization_percentage), end='\r')
        return (output_lines, row_errors)

    def process(self):
        """
        This should return a status code with appropriate files
        """
        try:

            # Check file prior to processing
            if self.verbose:
                print("Checking File Assumptions")
            self._check_file_assumptions()

            # Check adapters has right clean functions
            if self.verbose:
                print("Checking Clean Methods")
            self._check_clean_methods()

            if self.verbose:
                print("Beginning File Processing")

            with open(self.temp_file) as csvfile:
                reader = csv.DictReader(csvfile)
                lines = list(reader)

            # Process file
            if self.verbose:
                print("Starting Transformation/Clean")
            lines, row_errors = self._clean_lines(lines)

            if self.verbose:
                print("Finished Transformation/Clean")

            # Output Error File for Manual Review
            errors_filename = '%s_errors.csv' % self.file_name

            if len(row_errors) > 0:
                errors_file = utils.write_line_dict_to_csv(
                    errors_filename, row_errors)
                self.status = constants.ETL_FILE_PROCESSED_W_ERROR

                if self.verbose:
                    print("Outputting Error File:{}".format(errors_filename))
            else:
                self.status = constants.ETL_FILE_PROCESSED

            # Output normalized file
            if len(lines) == 0:
                raise parse_exceptions.NormalizationFailedError()

            normalized_filename = '%s_normalized.csv' % self.file_name
            normalized_file = utils.write_line_dict_to_csv(
                normalized_filename, lines)
            if self.verbose:
                print("Outputting Transformed File:{}".format(normalized_filename))

            return (self.status, normalized_file)



        except parse_exceptions.FileFormatError as e:
            print(e)
            return (constants.ETL_FILE_ERROR, e)

        except parse_exceptions.MissingFileError as e:
            print(e)
            return (constants.ETL_FILE_MISSING, e)

        except parse_exceptions.MissingCleanFuncError as e:
            print(e)
            return (constants.ETL_FILE_ERROR, e)

        except parse_exceptions.NormalizationFailedError as e:
            print(e)
            return (constants.ETL_FILE_ERROR, e)

