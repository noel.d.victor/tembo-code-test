CSV = 'csv'
XLS = 'xls'
EXCEL = 'excel'

CSV_MIMETYPES = (
    'text/csv',
)

XLS_MIMETYPES = (
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-excel',
)

ODS_MIMETYPES = (
    'application/vnd.oasis.opendocument.spreadsheet',
)

# ETL FILE CODES
ETL_FILE_PROCESSING = 1
ETL_FILE_ERROR = 2
ETL_FILE_INVALID = 3
ETL_FILE_PROCESSED = 5
ETL_FILE_PROCESSED_W_ERROR = 6
ETL_FILE_UNPROCESSED = 7
ETL_FILE_MISSING = 8

ETL_STATUS_CHOICES = (
    (ETL_FILE_UNPROCESSED, 'Unprocessed'),
    (ETL_FILE_PROCESSING, 'Processing'),
    (ETL_FILE_ERROR, 'Issues Found'),
    (ETL_FILE_INVALID, 'Invalid file'),
    (ETL_FILE_PROCESSED, 'Processed'),
    (ETL_FILE_PROCESSED_W_ERROR, 'Processed with Errors'),
    (ETL_FILE_MISSING, 'Missing File'),
)

# save memory by putting here for all the functions that will use it.
ETL_STATUS_CHOICES_DICT = dict(ETL_STATUS_CHOICES)
