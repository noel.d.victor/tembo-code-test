import tempfile
import csv
import mimetypes


def get_mimetype(filename):
    return mimetypes.MimeTypes().guess_type(filename)[0]


def write_line_dict_to_csv(fn, line_dict):
    keys = []
    for key in line_dict[0].keys():
        keys.append(key)

    with open(fn, 'w') as csvfile:
        fieldnames = keys
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for line in line_dict:
            writer.writerow(line)

    return fn


def preprocess_lines(lines):
    # lowercase all the stuff
    for line in lines:
        for item in line:
            line[line.index(item)] = item.lower()
    return lines


def export_file_to_tmp(nfile):
    # Export file to /tmp
    _, infn = tempfile.mkstemp()
    fin = open(nfile, 'rb')
    fout = open(infn, 'wb')

    fin.seek(0)
    fout.write(fin.read())

    fout.flush()
    fout.close()

    fin.flush()
    fin.close()

    return infn
