from .character_adapter import CharacterAdapter

import csv
import re

from .exceptions import FileAssumptionsChangedError
from .exceptions import InvalidRowError


class VillainAdapter(CharacterAdapter):
    slug = "villain_adapter_v1"

    def _check_file_assumptions(self):
        """
        Check our assumptions about file are still valid

        """
        super(VillainAdapter, self)._check_file_assumptions()
        expected_fields = ["id",
                           "name",
                           "IDENTITY",
                           "EYE",
                           "HAIR",
                           "SEX",
                           "ALIVE",
                           "AT LARGE",
                           "FIRST APPEARANCE",
                           "Year"]

        with open(self.temp_file, "r") as csvfile:
            reader = csv.reader(csvfile)
            fields = next(reader)

        for field in fields:
            if field not in expected_fields:
                raise FileAssumptionsChangedError()

    def clean_alignment(self, line):
        # Assuming all are villains in file
        value = "villian"
        return value

    def clean_at_large(self, line):
        orig_value = line['AT LARGE']
        return orig_value
