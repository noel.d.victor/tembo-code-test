from .hero_adapter import HeroAdapter
from .villain_adapter import VillainAdapter
from .stats_adapter import StatsAdapter

available_file_adapters = [
    HeroAdapter,
    VillainAdapter,
    StatsAdapter,
]

################

# make sure slugs are unique
slug_list = [x.slug for x in available_file_adapters]
found_identical_slugs = [x for x in slug_list if slug_list.count(x) > 1]

if len(found_identical_slugs) > 0:
    raise Exception("Check  Adapters: Identical Slug Found")

# set up variables used elsewhere
available_file_adapters_choices = [(x.slug, x.slug) for x in available_file_adapters]
available_file_adapters_choices.insert(0, (None, None))

available_adapter_slug_dict = {}
for adapter in available_file_adapters:
    available_adapter_slug_dict[adapter.slug] = adapter

################
