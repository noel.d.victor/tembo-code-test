import csv

import click
import rethinkdb as r

from settings import DATABASES
from settings import MARVEL_CHARS

from file_adapters import adapters
from file_adapters import utils

rethinkdb = DATABASES.get("rethinkdb")


@click.command()
def main():
    print("Beginning Initial Data Load")
    db_conn = r.connect(rethinkdb.host, rethinkdb.port)
    db_list = r.db_list().run(db_conn)
    if not rethinkdb.name in db_list:
        print("Database not found...Creating")
        r.db_create(rethinkdb.name).run(db_conn)

    table_list = r.db(rethinkdb.name).table_list().run(db_conn)
    if MARVEL_CHARS not in table_list:
        print("Table not found...Creating")
        r.db(rethinkdb.name).table_create(MARVEL_CHARS).run(db_conn)


    stats_adapter = adapters.StatsAdapter('./initial_data/marvel_stats.csv', verbose=True)
    status_code, normalized_stats = stats_adapter.process()

    hero_adapter = adapters.HeroAdapter('./initial_data/marvel_heroes.csv', verbose=True)
    status_code, normalized_heroes = hero_adapter.process()
    stitched_heros = join_files(normalized_heroes, normalized_stats)
    load_data(stitched_heros, r.db(rethinkdb.name), db_conn)

    villian_adapter = adapters.VillainAdapter('./initial_data/marvel_villains.csv', verbose=True)
    status_code, normalized_villians = villian_adapter.process()
    stitched_villains = join_files(normalized_villians, normalized_stats)
    load_data(stitched_villains, r.db(rethinkdb.name), db_conn)

    db_conn.close()
    print("Completed")


# for speed purposes join csv in files not db
def join_files(normalized_file, stats_file):
    print("Stitching {} and {}".format(normalized_file, stats_file))
    lines = []
    with open(normalized_file, "r") as csvfile:
        with open(stats_file) as stats_csvfile:
            row_no = 0
            reader = csv.DictReader(csvfile)
            stats_reader = list(csv.DictReader(stats_csvfile))
            for row in reader:
                print("Row: {} ".format(row_no), end='\r')
                row_no += 1
                row_id = row['id']
                for stats_line in stats_reader:
                    if stats_line['id'] == row_id:
                        for key, item in stats_line.items():
                            if item:
                                row[key] = item
                lines.append(row)
    stitched_filename = '%s_stitched.csv' % normalized_file
    utils.write_line_dict_to_csv(
        stitched_filename, lines)
    print("Finished: {}".format(stitched_filename))
    return stitched_filename


def load_data(filename, db, db_conn):
    print("Loading {}".format(filename))
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        status = db.table(MARVEL_CHARS).insert(list(reader)).run(db_conn,
                                                                 durability="soft", noreply=True)

    print("Finished {}".format(filename))


if __name__ == '__main__':
    main()
