## Tembo Python Performance Task
*Goal*: Transform and load source data into a NoSQL database; create a REST API that allows access to Marvel heroes, villains, and their stats.

### Source data
`marvel_heroes.csv`: Contains a list of Marvel heroes and their profile attributes.
`marvel_villains.csv`: Contains a list of Marvel villains
`stats.csv`: Contains stats for each of the X-Men and villains

### Expected results
* A NoSQL database (or script to create one) containing the transformed source data, accessible via a REST API.
* All transformation code/scripts for the ETL process.
* API should allow anyone to access the heroes, villains, and their associated statistics.
* Tests should be included that demonstrate the capabilities and limitations of the API you create.
* Implementation is expected to be completed in Python 3.
* You are encouraged to ask questions along the way.
* This task is meant to be completed in 2-4 hours. It's better to solve minimally than to spend more time adding features.