import rethinkdb as r

from settings import DATABASES
from settings import MARVEL_CHARS

from api.custom_exceptions import NoCharacterFoundError


rethinkdb = DATABASES.get("rethinkdb")


class DB_Connection(object):
    """
    Context manager for db connections
    """

    def __enter__(self):
        self.db_conn = r.connect(rethinkdb.host, rethinkdb.port)
        return self.db_conn

    def __exit__(self, arg, value, traceback):
        self.db_conn.close()


class MarvelCharacters(object):
    char_table = r.db(rethinkdb.name).table(MARVEL_CHARS)

    def get_all(self):
        with DB_Connection() as db_conn:
            values = self.char_table.run(db_conn)
            values = list(values)
        return values

    def get_one(self, char_id):
        with DB_Connection() as db_conn:
            values = self.char_table.filter({"id": str(char_id)}).run(db_conn)
            values = list(values)
            if not values:
                raise NoCharacterFoundError()
        return values[0]

    def create_new(self, data):
        with DB_Connection() as db_conn:
            values = self.char_table.insert(data).run(db_conn)
            char_id = values["generated_keys"][0]
            values = self.char_table.filter({"id": str(char_id)}).run(db_conn)
        values = list(values)
        return values[0]

    def update(self, char_id, data):
        with DB_Connection() as db_conn:
            values = list(self.char_table.filter({"id": str(char_id)}).run(db_conn))
            if values:
                self.char_table.filter({"id": str(char_id)}).update(data).run(db_conn)
            else:
                raise NoCharacterFoundError()
            values = list(self.char_table.filter({"id": str(char_id)}).run(db_conn))
            return values[0]

    def delete(self, char_id):
        with DB_Connection() as db_conn:
            values = list(self.char_table.filter({"id": str(char_id)}).run(db_conn))
            if values:
                self.char_table.filter({"id": str(char_id)}).delete().run(db_conn)
            else:
                raise NoCharacterFoundError()

        return values[0]
