#!/usr/bin/env bash

set -x
#redirect stdout and stderr to output file for easy review
#2>&1 need b/c unittest wraps around std stream
python /code/tests/test_character_api.py > /code/logs/tests_output.log 2>&1
