import logging
import json

from api.restplus import api
from flask import request
from flask_restplus import Resource
from api.restplus import api

from api.marvel_profiles.serializers import m_character
from api.marvel_profiles.serializers import no_id_character
from api.marvel_profiles.serializers import shortened_character

from database.models import MarvelCharacters
from api.custom_exceptions import NoCharacterFoundError

log = logging.getLogger(__name__)

ns = api.namespace('marvel_characters', description='Operations related to Marvel Characters')


@ns.route('/')
class CharacterCollection(Resource):
    @api.marshal_list_with(shortened_character)
    def get(self):
        """
        Returns list of Marvel Characters
        """
        characters = MarvelCharacters().get_all()
        return characters

    @api.response(200, 'Character successfully created.')
    @api.expect(no_id_character)
    @api.marshal_with(m_character)
    def post(self):
        """
        Creates a new Marvel Character
        """
        data = request.json
        new_character = MarvelCharacters().create_new(data)
        return new_character


@ns.route('/<id>')
@api.response(404, 'Character not found.')
class Character(Resource):

    @api.marshal_with(no_id_character)
    def get(self, id):
        """
        Returns Marvel Character Profile

        """
        character = MarvelCharacters().get_one(id)
        return character

    @api.expect(no_id_character)
    @api.marshal_with(no_id_character)
    @api.response(200, 'Character successfully updated.')
    def put(self, id):
        """
        Updates a Marvel Character.

        Use this method to  update a Marvel character

        * Send a JSON object with updated data

        * Specify the ID to modify in the request URL path.
        """
        data = request.json
        character = MarvelCharacters().update(id, data)
        return character, 200

    @api.response(200, 'Character successfully deleted.')
    def delete(self, id):
        """
        Deletes Marvel Character
        """
        character = MarvelCharacters().delete(id)
        body = json.dumps(["Deleted {0} : {1}".format(
            character["id"],
            character['name']
        )])

        return body, 200
