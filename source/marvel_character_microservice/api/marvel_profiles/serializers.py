from flask_restplus import fields
from api.restplus import api

shortened_character = api.model('Marvel Characters', {
    'id': fields.String(readOnly=True, description='The unique identifier of a marvel character'),
    'name': fields.String(description='Name'),

})

char_serials = {
    'id': fields.String(readOnly=True, description='The unique identifier of a marvel character'),
    'name': fields.String(description='Name', required=True),
    'nickname': fields.String(description='Nickname'),
    'extra': fields.String(description='An Extra Character Details'),
    'identity': fields.String(description='Secret Identity'),
    'marvel_universe': fields.String(description='Marvel Universe(w planet)'),
    'alignment': fields.String(description='Hero/Neutral/Villain'),
    'eye': fields.String(description='Eye Details'),
    'hair': fields.String(description='Hair Details'),
    'sex': fields.String(description='Sex'),
    'alive': fields.String(description='Alive/Dead'),
    'first_appearance': fields.String(description='First Appearance'),
    'at_large': fields.String(description='At Large'),
    'as_of_year': fields.String(description='Year Aggregates Last Updated'),
    'crimes_committed': fields.String(description='Crimes'),
    'estimated_damages': fields.String(description='Criminal Damages'),
    'total_appearances': fields.String(description='Total Appearances'),
}

no_id_serials = {x: y for x, y in char_serials.items() if x != "id"}

m_character = api.model('Marvel Character', char_serials)
no_id_character = api.model('Marvel Character', no_id_serials)
