import logging
import traceback

from flask_restplus import Api
from api.custom_exceptions import NoCharacterFoundError

log = logging.getLogger(__name__)

api = Api(version='1.0', title='My Marvel Universe API',
          description='A simple demonstration of a Flask RestPlus powered API')


@api.errorhandler(NoCharacterFoundError)
def not_found_error_handler(e):
    log.warning(traceback.format_exc())
    return {'message': 'A database result was required but none was found.'}, 404
