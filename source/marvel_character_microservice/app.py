import os
import sys
import logging
import logging.handlers

from flask import Flask, Blueprint

import settings

from api.marvel_profiles.endpoints.characters import ns as character_namespace
from api.restplus import api



formatter = logging.Formatter("%(asctime)s;%(levelname)s;%(message)s",
                              "%Y-%m-%d %H:%M:%S")
handler = logging.handlers.RotatingFileHandler(
    './logs/app.log',
    maxBytes=1024 * 1024)
handler.setLevel(logging.INFO)
handler.setFormatter(formatter)

app = Flask(__name__)

app.logger.addHandler(handler)


def configure_app(flask_app):
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = settings.RESTPLUS_ERROR_404_HELP


def initialize_app(flask_app):
    configure_app(flask_app)
    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(character_namespace)
    flask_app.register_blueprint(blueprint)


def main():
    initialize_app(app)
    # set the app logger level
    app.logger.setLevel(logging.INFO)

    app.logger.info('Starting development server at http://{}/api/'.format(
        app.config['SERVER_NAME']))

    app.run(debug=True, host='0.0.0.0', port=5000)



if __name__ == "__main__":
    main()
