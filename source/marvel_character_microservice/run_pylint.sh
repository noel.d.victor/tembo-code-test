#!/usr/bin/env bash

set -x
#redirect stdout and stderr to output file for easy review
cd ..
pylint --ignore=tests,tests.py --rcfile=/code/.pylintrc /code/ > /code/logs/pylint_summary.log 2>&1
