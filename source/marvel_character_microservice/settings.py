from collections import namedtuple

# Flask settings
FLASK_SERVER_NAME = 'localhost:8888'
FLASK_DEBUG = True  # Do not use debug mode in production

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False

Database_Conn = namedtuple('Database_Conn', ['name', 'host', 'port'])

DATABASES = {
    'rethinkdb': Database_Conn("marvel_universe", "rethinkdb", 28015),

}

# Table for characters
MARVEL_CHARS = "characters"
