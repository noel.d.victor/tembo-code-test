# Noel's Tembo Code Test

# Main Objective
To complete Tembo's Python Performance task.
See in ```source/marvel_character_microservice/initial_data/marvel_api_task```


# Technologies Used
- Docker
- Docker-Compose
- Python 3.6
- Flask (Flask RestPlus)
- Rethink (NoSQL DB)


# Screenshots
## Api with Flask-Restful Plus
 ![](flask-restfulplus-api.png) 

# Getting Started

#### Setup Microservice
- Goto the microservice directory
- ```docker-compose build```
- ```docker-compose up```
- ```docker-compose run marvel_microservice /code/load_data.sh```


You should be able to access the flask restplus powered api at the following:
- ```http://localhost:5000/api```

On this page, you can play with the api in the browser.

You also should be able to access Rethinkdb admin web tool here :
- ```http://localhost:8080/```


# Run Tests

#### Run Microservice Test
- ```docker-compose run marvel_microservice /code/run_tests.sh```

The tests were very simple. I'm still not quite sure how to structure flask tests.
They run against the live dev site and db.
This should also create a summary log.
- ```tests_output.log```


# Code Quality Checks
The pylint scripts should generate summary logs and use .pylintrc files in their respective dirs.
I don't use them as the word of god, but I generally use it to keep the my personal projects code quality 8 or higher.

Log
- ```pylint_summary.log```

#### Code Quality Checks on Microservice

- ```docker-compose run marvel_microservice /code/run_pylint.sh```

# Tips/Tricks/Troubleshooting


#### How do I get a high level view of the data before coding ?
I typically like to use _openrefine_ to get a look at messy data prior to ETL. 
- Use Facets to see variation within data fields
- From there you can use clustering
    - in the marvel dataset you can use clustering on the names to find possible dupes
    - ex Michael Van Patrick appears 4 times with slight variations
    - but I don't know enough about marvel to know why or what this means
    - ![](openrefine_example.png) 
- http://openrefine.org/index.html

#### Question : How do I debug the REST API effectively ?

Answer: You should probably use an REST API debug tool to make life easier.
You can use curl, but it will probably get annoying.
There are lots of gui tools like postman, telerik fiddler, and stoplight.
**The nice thing about flask-restplus is you can play around with it in the browswer**



#### Question: How do I get into bash on a Docker Container ?
Answer:

- `docker-compose run <service> <script>`
- `docker-compose run <service> bash`
- It might be useful if you just want to test some scripts

#### Question: Is there a GUI for rethink ?
Yes, there is a web interface.
It should be on http://localhost:8080/.
If its not working, double check the docker port settings.

https://www.rethinkdb.com/docs/administration-tools/


## Further Refs:
https://en.wikipedia.org/wiki/List_of_HTTP_status_codes

Curated list of Flask Libraries
- https://project-awesome.org/humiaozuzu/awesome-flask

Rethink 
- https://www.rethinkdb.com/
- https://www.rethinkdb.com/api/python/#db_list

Flask-Resful-Plus
- http://flask-restplus.readthedocs.io/en/stable/